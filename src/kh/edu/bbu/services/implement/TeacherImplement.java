package kh.edu.bbu.services.implement;

import kh.edu.bbu.models.Teacher;
import kh.edu.bbu.services.TeacherService;
import kh.edu.bbu.utils.TableGenerator;

import java.util.ArrayList;
import java.util.List;

public class TeacherImplement implements TeacherService {
    List<Teacher> teacherList = new ArrayList<>();
    List<Teacher> searchList = new ArrayList<>();
    @Override
    public void addTeacher(Teacher teacher) {
        teacherList.add(teacher);
    }

    @Override
    public void updateTeacher(Teacher teacher){
        for(Teacher teach:teacherList){
            if(teach.getId().equals(teacher.getId())){
                teacherList.set(teacherList.indexOf(teach), teacher);
                break;
            }
        }

    }

    @Override
    public void deleteTeacher(String id) {
        for(Teacher teach:teacherList){
            if(teach.getId().equals(id)){
                teacherList.remove(teach);
                break;
            }
        }
    }

    @Override
    public void searchTeacherById(String id) {
        searchList.clear();
        for(Teacher teach:teacherList){
            if(teach.getId().contains(id)){
                searchList.add(teach);
//                break;
            }
        }
        getAllTeachersWithTableGenerator(searchList);
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return teacherList;
    }

    @Override
    public void getAllTeachersWithTableGenerator(List<Teacher> list) {
        TableGenerator tableGenerator = new TableGenerator();
        List<String> headerList = new ArrayList<>();
        headerList.add("Id");
        headerList.add("Name");
        headerList.add("Gender");
        headerList.add("Age");
        headerList.add("Faculty");
        headerList.add("University");
        headerList.add("Phone");
        headerList.add("Email");
        headerList.add("Address");
        List<List<String>> rowsList = new ArrayList<>();
        list.forEach(c->{
            List<String> rows = new ArrayList<>();
            rows.add(c.getId());
            rows.add(c.getName());
            rows.add(c.getGender());
            rows.add(c.getAge());
            rows.add((c.getFaculty()));
            rows.add(c.getUniversity());
            rows.add(String.valueOf(c.getPhoneNumber()));
            rows.add(c.getEmail());
            rows.add(c.getAddress());
            rowsList.add(rows);
        });

        System.out.println(tableGenerator.generateTable(headerList, rowsList));
    }

    @Override
    public void menuOption() {
        System.out.println("List -> (1) || Add new -> (2) || Update -> (3) || Delete -> (4) || Search -> (5) || Exit -> (6)");
    }
}
