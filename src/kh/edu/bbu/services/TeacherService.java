package kh.edu.bbu.services;

import kh.edu.bbu.models.Teacher;

import java.util.List;

public interface TeacherService {
    void addTeacher(Teacher teacher);
    void updateTeacher(Teacher teacher);
    void deleteTeacher(String id);
    List<Teacher> getAllTeachers();
    void getAllTeachersWithTableGenerator(List<Teacher> list);
    void menuOption();
    void searchTeacherById(String Id);
}
